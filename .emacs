;;; .emacs -- Configuration file
;;; Commentary:
;;; Code:

;; Pour le bon usage des packages
(package-initialize)

;; Chemin de recherche

(setq load-path
      (append
       '(
	 "/home/phil/.lisp/"
	 )
       load-path))

;; Acc,Ah(Bs direct au r,Ai(Bpertoire courant avec C-x C-j
(require 'dired-x)

;;; Pour la souris ,A`(B roulette

(autoload 'mwheel-install "mwheel" "Enable mouse wheel support.")
(mwheel-install)

;; Pour les insertions de rectangles avec texte
;; (en remplacement de la commande qui fait
;; la m,Aj(Bme chose mais en overwritting)
(global-set-key "\C-xrt" 'string-insert-rectangle)

;; Plus vite pour tuer les fen,Aj(Btres
(global-set-key (kbd "<f11>") 'delete-other-windows)
(global-set-key (kbd "S-<f11>") 'delete-window)

;; Et d'autres raccourcis, bas,Ai(B f11 eux aussi (pour ,Ai(Bviter
;; les Key-chord qui semblent ralentir de fa,Ag(Bon sensible les
;; performances sous emacs lors de simples ,Ai(Bditions

(global-set-key (kbd "C-<f11> d")     'flyspell-mode)
(global-set-key (kbd "C-<f11> t")     'text-mode)
(global-set-key (kbd "M-<f11>")     'text-mode)
(global-set-key (kbd "C-<f11> a")     (lambda () (interactive) (ispell-change-dictionary "american")))
(global-set-key (kbd "C-<f11> f")     (lambda () (interactive) (ispell-change-dictionary "francais")))


;; Pour que les fichiers .tmp soient des fichiers textes
(setq auto-mode-alist
      (append
       '(
	 ("\\.tmp$" . text-mode)
	 ("\\.eml$" . text-mode)
	 )
       auto-mode-alist
       )
      )

; Les num,Ai(Bros de ligne qui apparaissent sur chaque ligne
(global-set-key (kbd "<f9>") 'linum-mode)

; Une compl,Ai(Btion un peu plus ,Ai(Bvolu,Ai(Be que celle par d,Ai(Bfaut
(global-set-key "\M-/" 'hippie-expand)

(setq hippie-expand-try-functions-list
      '(try-expand-all-abbrevs
	try-expand-dabbrev
	try-expand-list
	try-expand-line
	try-expand-dabbrev-all-buffers
	try-expand-dabbrev-from-kill
	try-complete-file-name-partially
	try-complete-file-name))


;; La compilation accessible facilement (C-RET)
(global-set-key [(control return)] 'compile)

; Le paste du bouton de milieu de la souris le fait au niveau
; du curseur, pas au niveau du pointeur de souris
(setq mouse-yank-at-point t)

; imenu partout o,Ay(B on peut : propose les noms de fonctions/m,Ai(Bthodes
; dans les sources, les titres dans les documents LaTeX, etc.
(global-set-key (kbd "C-:") 'imenu-anywhere)

; Affiner la taille de texte
(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C--") 'text-scale-decrease)

;;; Pour avoir un menu avec les fichiers r,Ai(Bcemment ouverts
(recentf-mode 1)
(setq recentf-max-saved-items 40)
(setq recentf-max-menu-items 40)
(setq recentf-exclude '(
                         "\\.ido.last$" ; ido mode (emacs)
                         "~$" ; emacs (and others) backup
                         "\\.log$" ; LaTeX
                         "\\.toc" ; LaTeX
                         "\\.el" ; Emacs Lisp (generally opened by the package manager)
                         "\\.eml" ; Le mail
                         "\\.aux$" ; LaTeX
                         "/COMMIT_EDITMSG$"
			 "/org/.*\\.org$"
                         ".el.gz$"
                         ))
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; Le bon mode en ,Ai(Bdition Web
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

;;; On affiche l'heure et le num,Ai(Bro de ligne
(display-time)
(line-number-mode t)

;; pour LaTeX
;(require 'tex-site)

; Pour un d,Ai(Bmarrage normal de ecb
(setq stack-trace-on-error t)

; Ido-mode, le mode qui va bien pour changer de buffer en
; montrant des suggestions
(setq ido-create-new-buffer 'always)
(setq ido-enable-flex-matching t)
(ido-mode 1)

; Une liste des buffers un peu plus compl,Ah(Bte
(defalias 'list-buffers 'ibuffer)

; Pour une ouverture intelligente des fichiers recherch,Ai(B en fonction du contexte
(require 'ffap)
(global-set-key (kbd "C-x C-g") 'ffap)
(global-set-key (kbd "C-x 5 g") 'ffap-other-frame)

; Une meilleure gestion des undo/redo
;; (require 'undo-tree)
;; (global-undo-tree-mode 1)

;;; pour une indentation intelligente en fonction des lignes precedentes
(require 'gin-mode)

;; Pour avoir un M-x un peu plus ,Ai(Bvolu,Ai(B
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; Une commande pour remonter la ligne courante en haut de l',Ai(Bcran
(defun my-recenter ()
  "Recenter page at almost the top."
  (interactive)
  (recenter 5)
)

(global-set-key "\C-p" 'my-recenter)

; Fly spelling
(autoload 'flyspell-mode "flyspell" "On-the-fly ispell." t)
(autoload 'global-flyspell-mode "flyspell" "On-the-fly spelling" t)

;; chargement automatique des "bons" modes
;; (appel,Ai(B par les modes LaTeX et Text
(add-hook 'text-mode-hook
	  '(lambda ()
	     (auto-fill-mode 1)
	     (gin-mode 1)
	     (ispell-change-dictionary "francais")
	     (flyspell-mode 1)))

; Gnuserv, remplac,Ai(B par emacsserver
(server-start)
;(global-set-key "\C-c\C-c" 'gnuserv-edit)

;; (autoload 'ruby-mode "ruby-mode"
;;    "Mode for editing ruby source files" t)
;; (setq auto-mode-alist
;;   (append '(("\\.rb$" . ruby-mode)) auto-mode-alist))
;; (setq interpreter-mode-alist (append '(("/usr/local/bin/ruby" . ruby-mode))
;;   interpreter-mode-alist))

;; (autoload 'run-ruby "inf-ruby"
;;   "Run an inferior Ruby process")
;; (autoload 'inf-ruby-keys "inf-ruby"
;;   "Set local key defs for inf-ruby in ruby-mode")
;; (add-hook 'ruby-mode-hook
;;       '(lambda ()
;;          (inf-ruby-keys)
;; ))

;; (setq load-path (cons "~/.lisp/emacs-rails" load-path))
;; (require 'rails)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(display-time-mode t)
 '(ecb-options-version "2.40")
 '(org-mobile-directory "/global/eldiablo/phil/Dropbox/org")
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#dddddd" :foreground "#141312" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 97 :width normal :foundry "unknown" :family "DejaVu Sans Mono"))))
 '(linum ((t (:inherit (shadow default) :width ultra-condensed))))
 '(web-mode-doctype-face ((t (:foreground "dark magenta"))))
 '(web-mode-html-attr-name-face ((t (:foreground "saddle brown"))))
 '(web-mode-html-tag-face ((t (:foreground "medium blue"))))
 '(web-mode-param-name-face ((t (:foreground "dim gray"))))
 '(web-mode-symbol-face ((t (:foreground "yellow1")))))
;; Pour auto-complete


(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 3)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)

(add-to-list 'load-path "~/.emacs.d/")
;; (require 'auto-complete)
;; (require 'auto-complete-config)
;; (ac-config-default)
;; (add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict") 
;; (global-auto-complete-mode t)

; ============================
; Passage Header-Source en C++
; ============================

(add-hook 'c++-mode-hook
  '(lambda ()
     (define-key c++-mode-map "\M-s" 'toggle-source)))

(add-hook 'c-mode-hook
  '(lambda ()
     (define-key c-mode-map   "\M-s" 'toggle-source)))

(defun toggle-visit-file (file-name)
;;
;  **********************************************************************
;  "Non interactive parameterized function to be used
;  to perform ISA-COM-FILE* functions. C Gerald Masini,
;  modified by toF Winkler"
;;;
;**********************************************************************

  (if (get-file-buffer file-name)
    (if (file-exists-p file-name)
  ;; --------------------------
  ;; both buffer and file exist
  ;; --------------------------
      (progn
        (find-file file-name)
	t
      )
  ;; -------------------------------------
  ;; buffer exists and file does not exist
  ;; -------------------------------------
      (find-file file-name)
      (message
        "Warning: File associated with buffer %s is not created."
        (buffer-name))
      (beep)
      t
    );;if
    (if (file-exists-p file-name)
  ;; -------------------------------------
  ;; buffer does not exist and file exists
  ;; -------------------------------------
      (progn
        (find-file file-name)
	t
      )
  ;; -----------------------------
  ;; neither buffer nor file exist
  ;; -----------------------------
      nil
  ;; -----------------------------
    );;if
  );;if
)


; Pour les polices et les couleurs
;(setq options-save-faces t)

(defun toggle-source()
  "Toggle C/C++ file from header to source file or vice versa"
  (interactive)
  (let ((bfn (buffer-file-name))
        (case-fold-search nil))
    (if bfn 
        (if ( or (string-match "\\.cc$" bfn)
		 (string-match "\\.c\\+\\+$" bfn)
		 (string-match "\\.c$" bfn)
		 (string-match "\\.C$" bfn)
		 (string-match "\\.H$" bfn) )
	  (let ((new-extension ".h"))
             (if (string-match "\\.C$" bfn)
		 (setq new-extension ".H"))
	     (if (string-match "\\.H$" bfn)
		 (setq new-extension ".C"))
             (setq new-bfn (replace-match new-extension t t bfn))
	     (if (not (toggle-visit-file new-bfn))
		 (error "Could not find file: %s" new-bfn))
          )
	  (if (string-match "\\.h$" bfn)
	      (if (not (toggle-visit-file (replace-match ".cc" t t bfn)))
		  (if (not (toggle-visit-file (replace-match ".c++" t t bfn)))
		      (if (not (toggle-visit-file (replace-match ".c" t t bfn)))
			  (error "Could not find any file corresponding to %s" bfn))
		  )
	      )
	      (error "File extension is not managed"))
        )
      )
   )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Configuration de Org

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq org-agenda-files (list "~/org"))

(global-set-key "\C-ca" 'org-agenda)
(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(global-set-key "\C-cl" 'org-store-link)
(put 'downcase-region 'disabled nil)

; TODO : ,A`(B faire, NEXT : en cours, DONE : fait, WAITING : en attente (+ note de quoi)
; HOLD : arr,Aj(Bt,Ai(B (ind,Ai(Btermin,Ai(B) / report,Ai(B, CANCELLED : annul,Ai(B + note pkoi

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold))))

; Pour g,Ai(Brer automatiquement des tags associ,Ai(Bs ,A`(B chaque ,Ai(Btat

(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING" . t) ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

; Pour les notes rapides
(setq org-directory "~/org")
(setq org-default-notes-file "~/org/refile.org")

;; I use C-c c to start capture mode
(global-set-key (kbd "C-c c") 'org-capture)

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
(setq org-capture-templates
      (quote (("t" "Todo" entry (file "~/org/refile.org")
               "* TODO %?\n%U\n" :clock-in nil :clock-resume nil)
              ;; ("r" "respond" entry (file "~/org/refile.org")
              ;;  "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
              ("n" "Note" entry (file "~/org/refile.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in nil :clock-resume nil)
              ("j" "Journal" entry (file+datetree "~/org/diary.org")
               "* %?\n%U\n" :clock-in nil :clock-resume nil)
              ("w" "Org-protocol" entry (file "~/org/refile.org")
               "* TODO Review %c\n%U\n" :immediate-finish t)
              ("m" "Meeting" entry (file "~/org/refile.org")
               "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
              ("p" "Phone call" entry (file "~/org/refile.org")
               "* PHONE %? :PHONE:\n%U" :clock-in nil :clock-resume nil)
              ("h" "Habit" entry (file "~/org/refile.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"<%Y-%m-%d %a .+1d/3d>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

; On note l'heure quand une t,Ab(Bche passe ,A`(B l',Ai(Btat termin,Ai(B
(setq org-log-done 'time)

;
; Pour le refiling
;

; Targets include this file and any file contributing to the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))

; Use full outline paths for refile targets - we file directly with IDO
(setq org-refile-use-outline-path t)

; Targets complete directly with IDO
(setq org-outline-path-complete-in-steps nil)

; Allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes (quote confirm))

; Use IDO for both buffer and file completion and ido-everywhere to t
(require 'flx-ido)
(ido-everywhere 1)
(flx-ido-mode 1)
;; disable ido faces to see flx highlights.
;(setq ido-use-faces nil)

(setq org-completion-use-ido t)
(setq ido-max-directory-size 100000)
(ido-mode (quote both))
; Use the current window when visiting files and buffers with ido
(setq ido-default-file-method 'selected-window)
(setq ido-default-buffer-method 'selected-window)
; Use the current window for indirect buffer display
(setq org-indirect-buffer-display 'current-window)

;;;; Refile settings
; Exclude DONE state tasks from refile targets
(defun bh/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets."
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(setq org-refile-target-verify-function 'bh/verify-refile-target)

;
; Agenda vue
;

(defvar bh/hide-scheduled-and-waiting-next-tasks t)

;; Do not dim blocked tasks
(setq org-agenda-dim-blocked-tasks nil)

;; Compact the block agenda view
(setq org-agenda-compact-blocks t)

;; Custom agenda command definitions
(setq org-agenda-custom-commands
      (quote (("N" "Notes" tags "NOTE"
               ((org-agenda-overriding-header "Notes")
                (org-tags-match-list-sublevels t)))
              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-overriding-header "Habits")
                (org-agenda-sorting-strategy
                 '(todo-state-down effort-up category-keep))))
              (" " "Agenda"
               ((agenda "" nil)
                (tags "REFILE"
                      ((org-agenda-overriding-header "Tasks to Refile")
                       (org-tags-match-list-sublevels nil)))
                (tags-todo "-CANCELLED/!"
                           ((org-agenda-overriding-header "Stuck Projects")
                            (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                            (org-agenda-sorting-strategy
                             '(priority-down category-keep))))
                (tags-todo "-HOLD-CANCELLED/!"
                           ((org-agenda-overriding-header "Projects")
                            (org-agenda-skip-function 'bh/skip-non-projects)
                            (org-agenda-sorting-strategy
                             '(priority-down category-keep))))
                (tags-todo "-CANCELLED/!NEXT"
                           ((org-agenda-overriding-header "Project Next Tasks")
                            (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                            (org-tags-match-list-sublevels t)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(priority-down todo-state-down effort-up category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING/!"
                           ((org-agenda-overriding-header (if (marker-buffer org-agenda-restrict-begin) "Project Subtasks" "Standalone Tasks"))
                            (org-agenda-skip-function 'bh/skip-project-tasks-maybe)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED+WAITING/!"
                           ((org-agenda-overriding-header "Waiting and Postponed Tasks")
                            (org-agenda-skip-function 'bh/skip-stuck-projects)
                            (org-tags-match-list-sublevels nil)
                            (org-agenda-todo-ignore-scheduled 'future)
                            (org-agenda-todo-ignore-deadlines 'future)))
                (tags "-REFILE/"
                      ((org-agenda-overriding-header "Tasks to Archive")
                       (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                       (org-tags-match-list-sublevels nil))))
               nil)
              ("r" "Tasks to Refile" tags "REFILE"
               ((org-agenda-overriding-header "Tasks to Refile")
                (org-tags-match-list-sublevels nil)))
              ("#" "Stuck Projects" tags-todo "-CANCELLED/!"
               ((org-agenda-overriding-header "Stuck Projects")
                (org-agenda-skip-function 'bh/skip-non-stuck-projects)))
              ("n" "Next Tasks" tags-todo "-WAITING-CANCELLED/!NEXT"
               ((org-agenda-overriding-header "Next Tasks")
                (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                (org-tags-match-list-sublevels t)
                (org-agenda-sorting-strategy
                 '(todo-state-down effort-up category-keep))))
              ("R" "Tasks" tags-todo "-REFILE-CANCELLED/!-HOLD-WAITING"
               ((org-agenda-overriding-header "Tasks")
                (org-agenda-skip-function 'bh/skip-project-tasks-maybe)
                (org-agenda-sorting-strategy
                 '(category-keep))))
              ("p" "Projects" tags-todo "-HOLD-CANCELLED/!"
               ((org-agenda-overriding-header "Projects")
                (org-agenda-skip-function 'bh/skip-non-projects)
                (org-agenda-sorting-strategy
                 '(category-keep))))
              ("w" "Waiting Tasks" tags-todo "-CANCELLED+WAITING/!"
               ((org-agenda-overriding-header "Waiting and Postponed tasks"))
               (org-tags-match-list-sublevels nil))
              ("A" "Tasks to Archive" tags "-REFILE/"
               ((org-agenda-overriding-header "Tasks to Archive")
                (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                (org-tags-match-list-sublevels nil))))))

(setq org-clock-out-remove-zero-time-clocks t)
(setq org-stuck-projects (quote ("" nil nil "")))

; NEXT est pour les t,Ab(Bches, pas pour les projets

(defun bh/mark-next-parent-tasks-todo ()
  "Visit each parent task and change NEXT states to TODO."
  (let ((mystate (or (and (fboundp 'org-state)
                          state)
                     (nth 2 (org-heading-components)))))
    (when mystate
      (save-excursion
        (while (org-up-heading-safe)
          (when (member (nth 2 (org-heading-components)) (list "NEXT"))
            (org-todo "TODO")))))))

(add-hook 'org-after-todo-state-change-hook 'bh/mark-next-parent-tasks-todo 'append)
(add-hook 'org-clock-in-hook 'bh/mark-next-parent-tasks-todo 'append)

; Sauvegarde automatique toutes les heures

(run-at-time "00:59" 3600 'org-save-all-org-buffers)

; Pour les sauvegardes de notes

(setq org-archive-mark-done nil)
(setq org-archive-location "%s_archive::* Archived Tasks")

(defun bh/skip-non-archivable-tasks ()
  "Skip trees that are not available for archiving"
  (save-restriction
    (widen)
    ;; Consider only tasks with done todo headings as archivable candidates
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max))))
          (subtree-end (save-excursion (org-end-of-subtree t))))
      (if (member (org-get-todo-state) org-todo-keywords-1)
          (if (member (org-get-todo-state) org-done-keywords)
              (let* ((daynr (string-to-int (format-time-string "%d" (current-time))))
                     (a-month-ago (* 60 60 24 (+ daynr 1)))
                     (last-month (format-time-string "%Y-%m-" (time-subtract (current-time) (seconds-to-time a-month-ago))))
                     (this-month (format-time-string "%Y-%m-" (current-time)))
                     (subtree-is-current (save-excursion
                                           (forward-line 1)
                                           (and (< (point) subtree-end)
                                                (re-search-forward (concat last-month "\\|" this-month) subtree-end t)))))
                (if subtree-is-current
                    subtree-end ; Has a date in this month or last month, skip it
                  nil))  ; available to archive
            (or subtree-end (point-max)))
        next-headline))))

; Export automatique vers Dropbox

(require 'gnus-async) 
(require 'org-mobile) 

;; Define a timer variable
(defvar org-mobile-push-timer nil
  "Timer that `org-mobile-push-timer' used to reschedule itself, or nil.")

;; Push to mobile when the idle timer runs out
(defun org-mobile-push-with-delay (secs)
  (when org-mobile-push-timer
    (cancel-timer org-mobile-push-timer))
  (setq org-mobile-push-timer
        (run-with-idle-timer
         (* 1 secs) nil 'org-mobile-push)))

;; After saving files, start an idle timer after which we are going to push
;; Defined at 5 mn (300 seconds)
(add-hook 'after-save-hook 
 (lambda () 
   (if (or (eq major-mode 'org-mode) (eq major-mode 'org-agenda-mode))
     (dolist (file (org-mobile-files-alist))
       (if (string= (expand-file-name (car file)) (buffer-file-name))
           (org-mobile-push-with-delay 300)))
     )))

;; Run after midnight each day (or each morning upon wakeup?) and each hour after that
(run-at-time "00:01" 3600 '(lambda () (org-mobile-push-with-delay 1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Une ligne s,Ai(Bparatrice de commentaire en LaTeX

(defun comtex ()
  (interactive)
  (let ((comsymb (read-from-minibuffer "Quel symbole: ")))
    (insert "%")
    (setq myiter 70)
    (while (> myiter 0)
      (setq myiter (- myiter 1))
      (insert comsymb)
      )
    (insert "\n")
    )
  )

(global-set-key "\C-xc" 'comtex)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Pour avoir une s,Ai(Bquence plus sympa de sortie des server-edit

(add-hook 'server-switch-hook
            (lambda ()
              (when (current-local-map)
                (use-local-map (copy-keymap (current-local-map))))
	      (when server-buffer-clients
		(local-set-key [(control *)] 'server-edit))))

; Pour el-get (remplacement / compl,Ai(Bment des gestionnaires de packages emacs)
;(require (cl-lib))

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(el-get 'sync)

; Des repos pour des packages suppl,Ai(Bmentaires
(setq package-archives '())

;; Add the original Emacs Lisp Package Archive
(add-to-list 'package-archives
             '("elpa" . "http://tromey.com/elpa/"))
;; Add the user-contributed repository
;; (add-to-list 'package-archives
;;              '("marmalade" . "http://marmalade-repo.org/packages/"))

(add-to-list 'package-archives
  '("melpa" . "http://melpa.milkbox.net/packages/") t)

(require 'package)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

;;;;; Pour magit

(global-set-key [(control !)] 'magit-status)
(require 'magit)
; Magit enregistre automatiquement les fichiers non sauv,Ai(Bs
(setq magit-save-some-buffers 'dontask)
; Quand rien n'est index,Ai(B, on indexe tout par d,Ai(Bfaut
(setq magit-commit-all-when-nothing-staged 'askstage)

; Autorevert par d,Ai(Bfaut 
(global-auto-revert-mode 1)

;; ;; Don't let magit-status mess up window configurations
;; ;; http://whattheemacsd.com/setup-magit.el-01.html
;; (defadvice magit-status (around magit-fullscreen activate)
;;   (window-configuration-to-register :magit-fullscreen)
;;   ad-do-it
;;   (delete-other-windows))

;; (defun magit-quit-session ()
;;   "Restores the previous window configuration and kills the magit buffer"
;;   (interactive)
;;   (kill-buffer)
;;   (when (get-register :magit-fullscreen)
;;     (ignore-errors
;;       (jump-to-register :magit-fullscreen))))

;; (define-key magit-status-mode-map (kbd "q") 'magit-quit-session)

;; ;; close popup when commiting

;; (defadvice git-commit-commit (after delete-window activate)
;;   (delete-window))

;; Pour les snippets : yas-global-mode
(global-set-key [(control $)] 'yas-global-mode)

;; (add-to-list 'load-path "/home/phil/.emacs.d/elpa/enh-ruby-mode-20130305.1652") ; must be added after any path containing old ruby-mode
;; (setq enh-ruby-program "/usr/bin/ruby") ; so that still works if ruby points to ruby1.8
;; (autoload 'enh-ruby-mode "enh-ruby-mode" "Major mode for ruby files" t)
;; (add-to-list 'auto-mode-alist '("\\.rb$" . enh-ruby-mode))
;; (add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))

;; Pour dire o,Ay(B est reveal...
(setq org-reveal-root "file:///home/dosch/divers/presentation/reveal.js")

;; Pour autocomplete

(require 'auto-complete)
(require 'auto-complete-config)
(global-auto-complete-mode t)
(setq ac-expand-on-auto-complete nil)
(setq ac-auto-start nil)
(setq ac-dwim nil) ; To get pop-ups with docs even if a word is uniquely completed

;;----------------------------------------------------------------------------
;; Use Emacs' built-in TAB completion hooks to trigger AC (Emacs >= 23.2)
;;----------------------------------------------------------------------------
(setq tab-always-indent 'complete)  ;; use 't when auto-complete is disabled
(add-to-list 'completion-styles 'initials t)

;; TODO: find solution for php, c++, haskell modes where TAB always does something

;; hook AC into completion-at-point
(defun sanityinc/auto-complete-at-point ()
  (when (and (not (minibufferp)) 
	     (fboundp 'auto-complete-mode)
	     auto-complete-mode)
    (auto-complete)))

(defun set-auto-complete-as-completion-at-point-function ()
  (add-to-list 'completion-at-point-functions 'sanityinc/auto-complete-at-point))

(add-hook 'auto-complete-mode-hook 'set-auto-complete-as-completion-at-point-function)


(set-default 'ac-sources
             '(ac-source-imenu
               ac-source-dictionary
               ac-source-words-in-buffer
               ac-source-words-in-same-mode-buffers
               ac-source-words-in-all-buffer))

(dolist (mode '(magit-log-edit-mode log-edit-mode org-mode text-mode haml-mode
                sass-mode yaml-mode csv-mode espresso-mode haskell-mode
                html-mode nxml-mode sh-mode smarty-mode clojure-mode
                lisp-mode textile-mode markdown-mode tuareg-mode
                js3-mode css-mode less-css-mode sql-mode ielm-mode))
  (add-to-list 'ac-modes mode))


;; Exclude very large buffers from dabbrev
(defun sanityinc/dabbrev-friend-buffer (other-buffer)
  (< (buffer-size other-buffer) (* 1 1024 1024)))

(setq dabbrev-friend-buffer-function 'sanityinc/dabbrev-friend-buffer)

;; Dired+
(require 'dired+)

;; Key-chord (d,Ai(Bsactiv,Ai(B car entra,An(Bne de lourds probl,Ah(Bmes de
;; de performances lors de simples ,Ai(Bditions sous emacs)

;; (require 'key-chord)
;; (key-chord-mode 1)

;; (key-chord-define-global "qs"     'delete-window)
;; (key-chord-define-global "wx"     'delete-other-windows)
;; (key-chord-define-global "*d"     'flyspell-mode)
;; (key-chord-define-global "*t"     'text-mode)
;; (key-chord-define-global "*a"     (lambda () (interactive) (ispell-change-dictionary "american")))
;; (key-chord-define-global "*f"     (lambda () (interactive) (ispell-change-dictionary "francais")))

;; Les curseurs multiples
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)

;; Pour helm (faire un choix avec ido/lcicle...)
;(require 'helm-config)

;; Des insertions d'espace sans bouger le curseur
(defun insert-postfix-whitespace ()
  "Just insert SPC symbol next to point."
  (interactive)
  (save-excursion
	(insert ?\s)
	(backward-char)))
 
;; whitespace next to cursor
(global-set-key (kbd "S-SPC") 'insert-postfix-whitespace)

;; Macro Google
(defun google ()
  "Google the selected region if any, display a query prompt otherwise."
  (interactive)
  (browse-url
   (concat
    "http://www.google.com/search?ie=utf-8&oe=utf-8&q="
    (url-hexify-string (if mark-active
         (buffer-substring (region-beginning) (region-end))
       (read-string "Search Google: "))))))
(global-set-key (kbd "C-x g") 'google)

;; Activation automatique de Flycheck
;(add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'ruby-mode-hook
	  '(lambda ()
	     (flycheck-mode 1)))
(add-hook 'emacs-lisp-mode-hook
	  '(lambda ()
	     (flycheck-mode 1)))
(add-hook 'c-mode-hook
	  '(lambda ()
	     (flycheck-mode 1)))
(add-hook 'web-mode-hook
	  '(lambda ()
	     (flycheck-mode 1)))

(global-set-key (kbd "C-<f11> c")     'flycheck-mode)

;; Pour feature-mode (,Ai(Bdition de fichier cucumber)
(require 'feature-mode)
(add-to-list 'auto-mode-alist '("\.feature$" . feature-mode))

;; Pour discover, le mode qui va bien pour pr,Ai(Bsenter certaines
;; options de commandes comme sous magit
;; (require 'discover)
;; (global-discover-mode 1)

;; Pour helm-swoop
(global-set-key (kbd "M-i") 'helm-swoop)
(global-set-key (kbd "M-I") 'helm-swoop-back-to-last-point)
(global-set-key (kbd "C-c M-i") 'helm-multi-swoop)
(global-set-key (kbd "C-x M-i") 'helm-multi-swoop-all)

(define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
(define-key isearch-mode-map (kbd "C-x M-i") 'helm-multi-swoop-all-from-isearch)
(setq helm-multi-swoop-edit-save t)

;; Pour helm-dash, la doc en local
(setq helm-dash-common-docsets '("HTML" "Java" "LaTeX" "Ruby")) 
(global-set-key (kbd "C-c d") 'helm-dash)

;; ido-vertical-mode
(ido-vertical-mode t)

;; 2048
;; (load-library '2048)
;; (global-set-key (kbd "C-2") '2048-play)

;; org-reveal
(require 'ox-reveal)
(setq org-reveal-root "file:///home/dosch/divers/presentation/reveal.js")

;; Ace-jump
(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)

;; Git personal macros
(fset 'gitprev
   [?\M-! ?g ?i ?t ?  ?p ?r ?e ?v return])

(fset 'gitnext
   [?\M-! ?g ?i ?t ?  ?n ?e ?x ?t return])

(global-set-key (kbd "<f10>")     'gitnext)
(global-set-key (kbd "S-<f10>")     'gitprev)

;; iedit, qui permet d',Ai(Bditer simultan,Ai(Bment des cha,An(Bnes identiques
;; avec C-;
(require 'iedit)

;;; .emacs ends here
(provide '.emacs)


